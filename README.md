# File Access Fix

## Public vs. private files

Drupal has locations for public and private files. Public files can be served
very fast by the webserver without any Drupal bootstrap needed, but you can not
have access control for them. Private files are served by Drupal, so can have
full access control, at the cost of being slower due to the additional Drupal
bootstrap.

# The problem of anonymous uploads

Think you have a public form where non-logged-in users can post testimonials
including some portrait file. And only after some editorial workflow they are 
published.

Now there's a dilemma: If you configure the file storage public, users or bots
can upload file content which is publicly exposed to the internet on your domain,
which you never audited. 
Security announcement [PSA\-2016\-003](https://www.drupal.org/psa-2016-003) is
about this and strongly advises storing files uploaded from untrusted users
in private file system only.
Otoh, if you follow that and configure file storage private, that security issue
is fixed - at the cost of site performance. If in our example, a campaign page
contains (say) 50 testimonial images, Drupal has on page load to serve 1 page plus
50 access controlled files.

## *File Access Fix* to the rescue

This module fixes this: In above example, the anonymously uploaded file lives in 
private file storage, until its node is published, at which time it is moved to
public storage.

## How the module does it

On entity save, for any file in any file field, public access is checked and
the file is moved to public / private storage, respectively.

Note that this is all-or-nothing: If a file has public access in any entity,
its it lives in public storage. Which is fine, as it's publicly accessible.

For files on media entities, this does not check the access of the entity or
entities that reference or embed that media. This is worked on in issue 
[#2904842: Make private file access handling respect the full entity reference chain](https://www.drupal.org/project/drupal/issues/2904842).

## Limitations

We have in this module no way of simulating different access contexts. Example:

* Have content with file field the access of which depend on domain, such that 
  anonymous user can access on domain foo but not on domain bar.
* Save the content on domain bar. This module will see no public access and 
  place the file in private storage. So the performance win of placing the file 
  in public storage is not gained, although it may be, as the content is 
  publicly accessible on domain foo.
* Save the content on domain foo. This module will see public acces and
  place the file in public storage. So the file will be publicly accessible on 
  domain foo as it should be. But as public storage is all-or-nothing, it will
  be publicly accessible on bar too.

If this is important to someone, probably the best way to fix this is to provide
a suitable alter hook that can be implemented by a custom module having site 
specific knowledge about access contexts. Also some helpers may improve DX, so
that implementors do not have to duplicaate too much code.

Patches will be appreciated.

## Help appreciated

This functionality imho should live in core and this module is a proof of
concept for issue [Automatically move files to public / private storage, depending on containing entity access](https://www.drupal.org/project/drupal/issues/3087660).
All effort should go there.

In the mean time, patches with tests are greatly appreciated and will be 
reviewed and committed.

Have fun!
[geek.merlin](https://www.drupal.org/user/229048)
