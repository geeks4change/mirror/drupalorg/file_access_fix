<?php

namespace Drupal\file_access_fix;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\user\Entity\User;

class FileAccessFixEntityHooks {

  /**
   * Hook_[insert|update]
   *
   * This moves files to private/public filesystem depending on the access of
   * the referencing entity, the field ant the file itself.
   * @todo Ensure that we have a private filesystem in hook_requirements.
   *
   * Note that if any access result has a nontrivial cache context (e.g. domain)
   * this is not well-defined, i.e. file public status will change depending on
   * on which domain the entity is saved.
   * @todo Find a way to fix this. Maybe add an alter hook to let contrib add
   *   site specific knowledge.
   *
   * @see \Drupal\file\FileAccessControlHandler::checkAccess
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public static function hookPostSave(EntityInterface $entity) {
    /** @var FileSystemInterface $fileSystemService */
    $fileSystemService = \Drupal::service('file_system');
    if ($entity instanceof FieldableEntityInterface && !$entity instanceof FileInterface) {
      $entityHasAnonAccess = self::entityHasAnonAccess($entity);
      foreach ($entity->getFields(FALSE) as $field) {
        if ($field instanceof FileFieldItemList) {
          $fieldHasAnonAccess = $entityHasAnonAccess && self::fieldHasAnonAccess($field);
          /** @var \Drupal\file\FileInterface $file */
          foreach ($field->referencedEntities() as $file) {
            $uriScheme = $fileSystemService->uriScheme($file->getFileUri());
            if (($uriScheme !== 'public') && ($uriScheme !== 'private')) {
              continue;
            }
            $fileMayBePublic = $file->isPermanent() && self::entityHasAnonAccess($file)
              && ($fieldHasAnonAccess || self::anyFileUsageHasAnonAccess($file));
            $fileIsPublic = $uriScheme === 'public';
            if ($fileMayBePublic !== $fileIsPublic) {
              $newUriScheme = $fileMayBePublic ? 'public' : 'private';
              $newUriPath = file_uri_target($file->getFileUri());
              $fileTargetUri = "$newUriScheme://$newUriPath";
              // Ensure that the target directory is created.
              $fileSystemService->prepareDirectory(dirname($fileTargetUri), FileSystemInterface::CREATE_DIRECTORY);
              // This will notify modules, e.f. for image derivations.
              $movedFile = file_move($file, $fileTargetUri, FileSystemInterface::EXISTS_RENAME);
              if (!$movedFile) {
                \Drupal::logger('file_accessfix')->error('Error moving file at @uri.', ['@uri' => $file->getFileUri()]);
              }
            }
          }
        }
      }
    }
  }

  /**
   * Check if any file usage can be accessed by anonymous user.
   *
   * @todo Use file_get_file_references() for this.
   *
   * @param \Drupal\file\FileInterface $file
   *
   * @return bool
   */
  private static function anyFileUsageHasAnonAccess(FileInterface $file) {
    // Adapted from \Drupal\file\FileAccessControlHandler::checkAccess
    $references = file_get_file_references($file);
    foreach ($references as $field_name => $entity_map) {
      foreach ($entity_map as $referencing_entity_type => $referencing_entities) {
        /** @var \Drupal\Core\Entity\EntityInterface $referencing_entity */
        foreach ($referencing_entities as $referencing_entity) {
          if (
            self::entityHasAnonAccess($referencing_entity)
            && self::fieldHasAnonAccess($referencing_entity->$field_name)
          ) {
            return TRUE;
          }
        }
      }
    }
    return FALSE;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return bool
   */
  private static function entityHasAnonAccess(EntityInterface $entity) {
    return $entity->access('view', User::getAnonymousUser());
  }

  /**
   * @param \Drupal\Core\Field\FieldItemListInterface
   *
   * @return bool
   */
  private static function fieldHasAnonAccess(FieldItemListInterface $field) {
    return $field->access('view', User::getAnonymousUser());
  }

}
